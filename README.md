# AutoScalableSwarm #

## Introduction ##

This document describes how the *Swarm Cluster* is designed, how it works and how it can be deployed.

The objective of this project is to create an Auto Scalable Swarm Cluster with Docker Machine in a Digital Ocean Cloud.

The Control Server will check cluster state and if necessary will start a new container or new server.

## Architecture Design ##

In the next diagram you can see the objective architecture.


![ServerDiagram.png](https://bitbucket.org/repo/rEARbX/images/152046755-ServerDiagram.png)


## CoreOS - the Operating System

[CoreOS](https://coreos.com/) is a new Linux distribution that has been rearchitected to provide features needed to run modern infrastructure stacks. The strategies and architectures that influence CoreOS allow companies like Google, Facebook and Twitter to run their services at scale with high resilience.

It utilizes an active/passive dual-partition scheme to update the OS as a single unit instead of package by package. This makes each update quick, reliable and able to be easily rolled back.

## Cloud-config - the OS configuration

CoreOS allows you to declaratively customize various OS-level items, such as network configuration, user accounts, and systemd units. [This document](https://coreos.com/docs/cluster-management/setup/cloudinit-cloud-config/) describes the full list of items we can configure. The coreos-cloudinit program uses these files as it configures the OS after startup or during runtime.

We bootstrap the CoreOS machines using cloud-config files. We use CoreOS for Control server and Proxy server. For Swarm Cluster it is not possible to use CoreOS yet and we have to use Ubuntu. 

## etcd - the distributed key value store

[etcd](https://coreos.com/docs/distributed-configuration/getting-started-with-etcd/) is an open-source distributed key value store that provides shared configuration and service discovery for CoreOS clusters. etcd runs on each machine in a cluster and gracefully handles master election during network partitions and the loss of the current master.

We use etcd to manage the cluster variables. 

To manage etcd, use the `etcdctl` tool to retrieve and set variables.


## docker-machine

[Machine](https://docs.docker.com/machine/) takes you from “zero-to-Docker” with a single command. It lets you easily deploy Docker Engines on your computer, on cloud providers, and in your own data center.

We use docker machine to deploy new servers on DigitalOcean. When the cluster runs out of resources we can start a new server with only one command.

## docker-swarm

[Swarm](https://docs.docker.com/swarm/) is native clustering for Docker containers. It pools together several Docker Engines into a single, virtual host. Point a Docker client or third party tool at Swarm and it will transparently scale to multiple hosts. 

We use swarm to deploy containers to our cluster. If the cluster runs out of resources we start a new container and swarm deploys this container in one node of the cluster. If we don't have enough resources we need to deploy a new Digital Ocean Server.

## Bootstrapping a cluster ##

To test this projects we need some pre-requisites.

* We need a [Digital Ocean Account](https://www.digitalocean.com/).
* We need to add our SSH public key to Digital Ocean. For [Linux Users](https://www.digitalocean.com/community/tutorials/how-to-use-ssh-keys-with-digitalocean-droplets) or for [Windows Users](https://www.digitalocean.com/community/tutorials/how-to-use-ssh-keys-with-putty-on-digitalocean-droplets-windows-users).
* We need to create a personal [acces token](https://www.digitalocean.com/community/tutorials/how-to-use-the-digitalocean-api-v2).

### Deploying Control Server ###

This server contains all the scripts needed to manage the cluster . In the /opt/bin directory we can find all the scripts.

* checkCpu.sh - This script controls the percentage of CPU being used in the cluster and if it exceeds certain thresholds it creates new containers or removes existing ones.
  
* startContainer.sh - This script creates a new container, and if it is not possible, creates a new virtual server.

* startServer.sh - This script creates a new virtual server and adds it to the swarm's farm.

* destroyServer.sh - This script stops containers of a server and then deletes the server from the farm and the virtual environment.


To deploy the new server we need to generate a new token for each unique cluster from https://discovery.etcd.io/new?size=1
We have to copy the result of the request on the seventh line of [cloud-init.yaml](https://bitbucket.org/urik23/autoscalableswarm/src/9bc06619eb737c17feea8a8d694f09a57994088d/cloud-config.yaml?at=master) that you have in the respository.

Now we can deploy our [Control Server](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-coreos-cluster-on-digitalocean) using the cloud-init.yaml created in the previous step.

After this we can connect to the server using our key and core user.

We have to verify these things.

* Docker
```
core@control ~ $ docker info
Containers: 0
Images: 0
Storage Driver: overlay
 Backing Filesystem: extfs
Execution Driver: native-0.2
Logging Driver: json-file
Kernel Version: 4.1.7-coreos-r1
Operating System: CoreOS 766.5.0
CPUs: 1
Total Memory: 493.5 MiB
Name: control
ID: OPPA:3SXB:YGMD:2LCU:YBUN:HP4O:E3NQ:V43V:6X52:MHPH:EI7R:6YF4
```


* Etcd

```
core@control ~ $ etcdctl set hello world
world
core@control ~ $ etcdctl get hello
world
core@control ~ $ etcdctl ls
/hello
core@control ~ $ etcdctl rm hello
```

* Docker Machine

```
core@control ~ $ docker-machine ls
NAME   ACTIVE   DRIVER   STATE   URL   SWARM
```

We need a swarm token. We have to execute these commands:

```
core@control ~ $ docker run --name tocken-machine swarm create
Unable to find image 'swarm:latest' locally
latest: Pulling from swarm

cf43a42a05d1: Pull complete
42c4e5c90ee9: Pull complete
22cf18566d05: Pull complete
048068586dc5: Pull complete
2ea96b3590d8: Pull complete
12a239a7cb01: Pull complete
26b910067c5f: Pull complete
e12f8c5e4c3b: Pull complete
swarm:latest: The image you are pulling has been verified. Important: image verification is a tech preview feature and should not be relied on to provide security.

Digest: sha256:51a30269d3f3aaa04f744280e3c118aea032f6df85b49819aee29d379ac313b5
Status: Downloaded newer image for swarm:latest
04aa4833886b2c7ca121a74998cf31cb
```

After this command we have to copy the token result: 04aa4833886b2c7ca121a74998cf31cb

Now we can delete the container. 

```
core@control ~ $ docker rm tocken-machine
tocken-machine
```

We have to publish some information into etcd to consume in the cluster. These are the variables that we need:

* SWARM_TOKEN - Token for swarm cluster
* SERVER_NUM - The number of swarm nodes
* VHOST - A name that NGINX uses to group all the servers for the reverse proxy
* DIGITAL_OCEAN_TOKEN - Token for access Digital Ocean API
* MAX_CPU_CORE - Average of the maximum cores that we can use. If the average exceeds this number of cores we launch a new server
* MIN_CPU_CORE - Average of the minimum cores that we can use. If the average is below this number of cores we destroy one server

To set these variables we need to execute these commands:

```
core@control ~ $ etcdctl set /GLOBAL/SWARM_TOKEN token://04aa4833886b2c7ca121a74998cf31cb
04aa4833886b2c7ca121a74998cf31cb
core@control ~ $ etcdctl set /GLOBAL/SERVER_NUM 1
1
core@control ~ $ etcdctl set /GLOBAL/VHOST www.plujadepapers.cat
www.plujadepapers.cat
core@control ~ $ etcdctl set /GLOBAL/DIGITAL_OCEAN_TOKEN 6a96d93550c7f6dd4e89472b2f0974d34687cf7ea7489d95cd41d42720a03967
6a96d93550c7f6dd4e89472b2f0974d34687cf7ea7489d95cd41d42720a03967
core@control ~ $ etcdctl set /GLOBAL/MAX_CPU_CORE 2
2
core@control ~ $ etcdctl set /GLOBAL/MIN_CPU_CORE 1
1
```

At this point we can start the swarm master node.
```
core@control ~ $ export DIGITAL_OCEAN_TOKEN=`etcdctl get /GLOBAL/DIGITAL_OCEAN_TOKEN`
core@control ~ $ export SWARM_TOKEN=`etcdctl get /GLOBAL/SWARM_TOKEN`
core@control ~ $ docker-machine create -d digitalocean --digitalocean-region "lon1" --digitalocean-size "512mb" --digitalocean-access-token $DIGITAL_OCEAN_TOKEN --swarm --swarm-master --swarm-discovery $SWARM_TOKEN swarm-master
Creating CA: /home/core/.docker/machine/certs/ca.pem
Creating client certificate: /home/core/.docker/machine/certs/cert.pem
Running pre-create checks...
Creating machine...
Waiting for machine to be running, this may take a few minutes...
Machine is running, waiting for SSH to be available...
Detecting operating system of created instance...
Provisioning created instance...
Copying certs to the local machine directory...
Copying certs to the remote machine...
Setting Docker configuration on the remote daemon...
Configuring swarm...
To see how to connect Docker to this machine, run: docker-machine env swarm-master
```

We have to wait for a few minutes before the process finishes. When it is done we can check the installation with these commands
```
core@control ~ $ docker-machine ls
NAME           ACTIVE   DRIVER         STATE     URL                        SWARM
swarm-master   -        digitalocean   Running   tcp://46.101.38.188:2376   swarm-master (master)
core@control ~ $ eval "$(docker-machine env --swarm swarm-master)"
core@control ~ $ docker info
Containers: 2
Images: 1
Role: primary
Strategy: spread
Filters: health, port, dependency, affinity, constraint
Nodes: 1
 swarm-master: 46.101.38.188:2376
  └ Containers: 2
  └ Reserved CPUs: 0 / 1
  └ Reserved Memory: 0 B / 514.5 MiB
  └ Labels: executiondriver=native-0.2, kernelversion=3.13.0-57-generic, operatingsystem=Ubuntu 14.04.3 LTS, provider=digitalocean, storagedriver=aufs
CPUs: 1
Total Memory: 514.5 MiB
Name: 9ae9cb460b74
```
At this point we finish the deployment of Control Server

### Deploy of NGINX Proxy ###

We can deploy our [NGINX Proxy](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-coreos-cluster-on-digitalocean) using the same cloud-init.yaml created for Control Server.

After this we can connect to the server by using our key and core user.

In this server we run a container with NGINX proxy. We want this container to update the configuration every time we start or stop a machine in the swarm cluster. For this reason we need the certificates generated in the Control Server to access the swarm master. To do this we have to execute these commands:

In NGINX Proxy
```
core@nginx ~ $ sudo passwd core
Changing password for core
Enter the new password (minimum of 5 characters)
Please use a combination of upper and lower case letters and numbers.
New password:
Re-enter new password:
passwd: password changed.
core@nginx ~ $ mkdir -p /home/core/.docker/machine/certs/
```

In Control server. You need to substitute the destination IP by the IP of your NGINX Server

```
core@control ~ $ sudo scp /home/core/.docker/machine/certs/* core@46.101.58.206:/home/core/.docker/machine/certs/
The authenticity of host '46.101.58.206 (46.101.58.206)' can't be established.
ED25519 key fingerprint is d8:03:2a:0a:83:67:62:3a:23:f7:c1:2a:5f:68:3e:85.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '46.101.58.206' (ED25519) to the list of known hosts.
core@46.101.58.206's password:
ca-key.pem                                                                                                                                                      100% 1679     1.6KB/s   00:00
ca.pem                                                                                                                                                             100% 1029     1.0KB/s   00:00
cert.pem                                                                                                                                                           100% 1054     1.0KB/s   00:00
key.pem                                                                                                                                                            100% 1679     1.6KB/s   00:00
core@control ~ $
```

Now we can start the NGINX container
`docker run -d --name nginx-proxy -v /home/core/.docker/machine/certs:/certs/ -p 80:80 --env ENDPOINT_SWARM_MASTER=tcp://46.101.58.219:3376 urik/nginx-proxy-swarm`

Before launching this command you need to substitute the IP of the ENDPOINT_SWARM_MASTER by the IP  of your Swarm Master.

In the Control Server execute:

```
core@control /opt/bin $ docker-machine ls
NAME           ACTIVE   DRIVER         STATE     URL                       SWARM
swarm-master   -        digitalocean   Running   tcp://178.62.90.97:2376   swarm-master (master)
```

This is the IP that you need to pass in ENDPOINT_SWARM_MASTER variable.

```
core@nginx ~ $ docker run -d --name nginx-proxy -v /home/core/.docker/machine/certs:/certs/ -p 80:80 --env ENDPOINT_SWARM_MASTER=tcp://46.101.58.219:3376 urik/nginx-proxy-swarm
Unable to find image 'urik/nginx-proxy-swarm:latest' locally
latest: Pulling from urik/nginx-proxy-swarm

575489a51992: Pull complete
6845b83c79fb: Pull complete
d0ea6b0b9e6a: Pull complete
77f88505fa0d: Pull complete
98de8af78960: Pull complete
fadf699bccc0: Pull complete
5ac100425925: Pull complete
738e7a471e87: Pull complete
e240c7325698: Pull complete
5cd1c00ad84f: Pull complete
c1ff64fa1aa6: Pull complete
3669a1f15a6f: Pull complete
5f58bca7ed48: Pull complete
6dfa6370e475: Pull complete
05f650202c49: Pull complete
444614c7456c: Pull complete
0a38772cccb1: Pull complete
280f53f8930f: Pull complete
06dbc304a979: Pull complete
bef4c776861a: Pull complete
1cf109b84057: Pull complete
d2c525429f30: Pull complete
f16a00610623: Pull complete
5ddb4ce45450: Pull complete
d034a60ee0cd: Pull complete
87b142804465: Already exists
Digest: sha256:43d5e960698c349115d3c416ef80f27d5f08086e81da6f1ad1dfc3924f80089d
Status: Downloaded newer image for urik/nginx-proxy-swarm:latest
9c94e9e9415de573988fd5b0220e43104a04c7ec98583066f873f48c02fb43d9
core@nginx ~ $ docker ps
CONTAINER ID        IMAGE                    COMMAND             CREATED             STATUS              PORTS                         NAMES
9c94e9e9415d        urik/nginx-proxy-swarm   "/opt/bin/run.sh"   11 seconds ago      Up 10 seconds       0.0.0.0:80->80/tcp, 443/tcp   nginx-proxy
```

At this point we have our cluster prepared for testing.

## Test Cluster ##

Now we have the initial structure of the cluster.
We need to start a container with `startContainer.sh`

We can check that it is online with `docker ps`

```
core@control ~ $ docker-machine ls
NAME           ACTIVE   DRIVER         STATE     URL                        SWARM
swarm-master   -        digitalocean   Running   tcp://46.101.30.223:2376   swarm-master (master)
core@control ~ $ eval "$(docker-machine env --swarm swarm-master)"
core@control ~ $ docker ps
CONTAINER ID        IMAGE               COMMAND                CREATED              STATUS              PORTS                           NAMES
be5fe878bc10        urik/web-uoc        "java -Djava.securit   About a minute ago   Up About a minute   46.101.30.223:32768->8080/tcp   swarm-master/cranky_fermat
```


And the container is added to the NGINX Proxy with:
`http://ip_nginx_server`

If we want to start another container we can run again `startContainer.sh`
Now with `docker ps` we have two containers.


```
core@control ~ $ startContainer.sh
core@control ~ $ docker ps
CONTAINER ID        IMAGE               COMMAND                CREATED              STATUS              PORTS                           NAMES
524484a32dc7        urik/web-uoc        "java -Djava.securit   3 seconds ago        Up 2 seconds        46.101.30.223:32769->8080/tcp   swarm-master/hungry_swirles
be5fe878bc10        urik/web-uoc        "java -Djava.securit   About a minute ago   Up About a minute   46.101.30.223:32768->8080/tcp   swarm-master/cranky_fermat
core@control ~ $
```

If we check http://ip_nginx_server we can see that the NGINX shows both containers. Every time we refresh the screen the name of the container changes.

Container 524484a32dc7

![2015-11-17 21_36_17-Test UOC PFC.png](https://bitbucket.org/repo/rEARbX/images/195396585-2015-11-17%2021_36_17-Test%20UOC%20PFC.png)

Container be5fe878bc10

![2015-11-17 21_36_36-Test UOC PFC.png](https://bitbucket.org/repo/rEARbX/images/3923037657-2015-11-17%2021_36_36-Test%20UOC%20PFC.png)

In the image column we can see that all the containers are running in the swarm master. 
If we try to start another container we need a new server because with our configuration we can run just two containers on a server.

```
core@control ~ $ startContainer.sh
Error response from daemon: no resources available to schedule container
1
1
5 secs. - 1 nodes swarm
10 secs. - 1 nodes swarm
15 secs. - 1 nodes swarm
20 secs. - 1 nodes swarm
25 secs. - 1 nodes swarm
30 secs. - 1 nodes swarm
35 secs. - 1 nodes swarm
40 secs. - 1 nodes swarm
45 secs. - 1 nodes swarm
50 secs. - 1 nodes swarm
55 secs. - 1 nodes swarm
60 secs. - 1 nodes swarm
65 secs. - 1 nodes swarm
70 secs. - 1 nodes swarm
75 secs. - 1 nodes swarm
80 secs. - 1 nodes swarm
85 secs. - 1 nodes swarm
90 secs. - 1 nodes swarm
95 secs. - 1 nodes swarm
100 secs. - 1 nodes swarm
105 secs. - 1 nodes swarm
110 secs. - 1 nodes swarm
115 secs. - 1 nodes swarm
120 secs. - 1 nodes swarm
125 secs. - 1 nodes swarm
130 secs. - 1 nodes swarm
135 secs. - 2 nodes swarm
core@control ~ $
```
After finishing the process we have three containers. Two containers are running in the swarm master and the third one is running in swarm-node-1.

```
core@control ~ $ docker ps
CONTAINER ID        IMAGE               COMMAND                CREATED              STATUS              PORTS                           NAMES
7511b75034b8        urik/web-uoc        "java -Djava.securit   About a minute ago   Up About a minute   178.62.81.92:32768->8080/tcp    swarm-node-1/drunk_lovelace
524484a32dc7        urik/web-uoc        "java -Djava.securit   10 minutes ago       Up 10 minutes       46.101.30.223:32769->8080/tcp   swarm-master/hungry_swirles
be5fe878bc10        urik/web-uoc        "java -Djava.securit   12 minutes ago       Up 12 minutes       46.101.30.223:32768->8080/tcp   swarm-master/cranky_fermat
```

After all this operations in our DigitalOcean Control Panel we can see all the servers that we have.

![2015-11-17 21_45_34-DigitalOcean Control Panel.png](https://bitbucket.org/repo/rEARbX/images/1549080319-2015-11-17%2021_45_34-DigitalOcean%20Control%20Panel.png)

Now we can use checkCPU.sh to test the CPU and if necessary the script will start or stop a server. We can use jMeter to try to stress the server. We have and example file uoc_test.jmx in the repository.