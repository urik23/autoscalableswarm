# Scripts Control #

In this folder we have all the necessary scripts to run our cluster.

## Start Container ##

This script tries to start a new container. If the cluster doesn't have enough resources, launch another script to start a new server.

![StartContainer.png](https://bitbucket.org/repo/rEARbX/images/2975617800-StartContainer.png)


## Start Server ##

This script tries to create a new server in Digital Ocean and adds this new server to the Swarm Cluster. 

![StartServer.png](https://bitbucket.org/repo/rEARbX/images/2057231901-StartServer.png)

## Destroy Server ##

This script stops all the containers running on a server and then destroys the server from Digital Ocean.

![DestroyServer.png](https://bitbucket.org/repo/rEARbX/images/3049092801-DestroyServer.png)

## Check CPU ##

This script is checking the CPU. It keeps the average CPU of the last five executions . 
Calculates the average of these five recent performances and if it is above a threshold launches StartContainer script . 
If it is below it launches DestroyContainer script.

![Check CPU.png](https://bitbucket.org/repo/rEARbX/images/336791972-Check%20CPU.png)
