#!/bin/bash

if [ ! -e /opt/bin/serverOperation ]; then
eval "$(docker-machine env --swarm swarm-master)"


  sudo touch /opt/bin/acuMitja
  sudo chmod 777 /opt/bin/acuMitja
  lin=$(sudo wc -l < /opt/bin/acuMitja)

  if [ "$lin" -eq 5 ]; then
     sudo tail --lines=4 /opt/bin/acuMitja > /opt/bin/acuMitjaTemp
     sudo mv /opt/bin/acuMitjaTemp /opt/bin/acuMitja
  fi

  sudo chmod 777 /opt/bin/acuMitja

  docker ps -q | awk -f /opt/bin/cpu.awk -v lin=$lin >> /opt/bin/acuMitja

  mitja=$(awk '{total+=$1} END{print total/NR}' /opt/bin/acuMitja)

  echo $mitja
  max_core=`etcdctl get /GLOBAL/MAX_CPU_CORE`
  var=$(awk -v mitja=$mitja -v max_core=$max_core 'BEGIN{ if ( mitja > max_core ) {print 1} else {print 0} }')

  echo "var1 " $var
  if [ "$var" -eq 1 ]; then
    echo "StartContainer"
    sh startContainer.sh >> sudo /opt/bin/server.log &
  fi

  min_core=`etcdctl get /GLOBAL/MIN_CPU_CORE`
  var=$(awk -v mitja=$mitja -v min_core=$min_core 'BEGIN{ if ( mitja < min_core ) {print 1} else {print 0} }')
  echo "var2 " $var
  if [ "$var" -eq 1 ]; then
    serverNum=`etcdctl get /GLOBAL/SERVER_NUM`
    echo "Comprobem quants servidors tenim: " $serverNum
    if [ "$serverNum" -gt "1" ]; then
      echo "Destruim Servidor"
       sh destroyServer.sh >> sudo /opt/bin/server.log &
    fi
  fi
fi
