#!/bin/bash

sudo touch /opt/bin/serverOperation

eval "$(docker-machine env --swarm swarm-master)"

VHOST=`etcdctl get /GLOBAL/VHOST`

RESULTAT=$(docker run -d -p 8080 -m 200M --env VHOST=$VHOST urik/web-uoc) 

if [ -z "$RESULTAT" ]; 
then 
  nodesSwarmObj=$(docker info | grep Nodes | awk '{print $2}')
  nodesSwarm=$nodesSwarmObj
  sh startServer.sh >> sudo /opt/bin/server.log &
  segons=0
  echo $nodesSwarmObj
  echo $nodesSwarm
      while [ "$nodesSwarm" -eq "$nodesSwarmObj" ] && [ $segons -lt 300 ]; do
         sleep 5
         ((segons=segons+5))
         nodesSwarm=$(docker info | grep Nodes | awk '{print $2}')
         echo $segons "secs. -" $nodesSwarm "nodes swarm"
      done
  if [ "$nodesSwarm" -ne "$nodesSwarmObj" ]; then
    sh startContainer.sh >> sudo /opt/bin/server.log &
  fi
fi

sudo rm -rf /opt/bin/serverOperation
