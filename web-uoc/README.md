# web-uoc

To run this container for this project you need to force the memory utilization and set the environment variable VHOST.
This is obligatory if you want that NGINX autoupdates the configuration.

This is an example:

```
docker run -d -p 8080 -m 200M --env VHOST=$VHOST urik/web-uoc
```

When the container is up you can access it with different options.

`http://localhost/`
Returns a static page with Hello World message.

`http://localhost/fibonacci/X`
Returns the result of fibonacci sequence of X numbers. 

`http://localhost/pi/X`
Returns the PI number with X decimals.

`http://localhost/env`
Returns the environment variables

`http://localhost/metrics`
Returns the metrics and statistics on the container.
This method is used in the project to detect if the CPU load is increasing

`http://localhost/trace`
Returns the last calls

