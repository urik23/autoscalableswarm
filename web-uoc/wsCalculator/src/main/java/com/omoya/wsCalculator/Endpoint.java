package com.omoya.wsCalculator;


import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class Endpoint {
	
	@RequestMapping(value = "/")
    public String hello() {
		return generateWeb("hello", "world");
	}
	
	@RequestMapping(value = "/fibonacci/{numberStr}")
    public String fibonacci(@PathVariable String numberStr) {
		int number = Integer.parseInt(numberStr);
		String result = "";
		for (int i = 1; i <= number; i++) {
			result = result + fib(i) + "\n";
		}
		return generateWeb("fibonacci", result);
    }

	@RequestMapping(value = "/pi/{numberStr}")
    public String pi(@PathVariable String numberStr) {
        return generateWeb("pi", pi_digits(Integer.parseInt(numberStr)));
    }
	
	private static long fib(int n) {
		if (n <= 1)
			return n;
		else
			return fib(n - 1) + fib(n - 2);
	}
	
	private static final int SCALE = 10000;
    private static final int ARRINIT = 2000;

    private static String pi_digits(int digits){
        StringBuffer pi = new StringBuffer();
        int[] arr = new int[digits + 1];
        int carry = 0;

        for (int i = 0; i <= digits; ++i)
            arr[i] = ARRINIT;

        for (int i = digits; i > 0; i-= 14) {
            int sum = 0;
            for (int j = i; j > 0; --j) {
                sum = sum * j + SCALE * arr[j];
                arr[j] = sum%(j * 2 - 1);
                sum /= j * 2 - 1;
            }

            pi.append(String.format("%04d", carry + sum / SCALE));
            carry = sum%SCALE;
        }
        return pi.toString();
    }
    
    private String generateWeb(String methodCalled, String resultToAdd) {
    	String html = ""+ "\n";
    	html = html + "<html>"+ "\n";
    	html = html + "<head>"+ "\n";
    	html = html + "<title>Test UOC PFC</title>"+ "\n";
    	html = html + "<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>"+ "\n";
    	html = html + "<style>"+ "\n";
    	html = html + "body {"+ "\n";
    	html = html + "background-color: white;" + "\n";
    	html = html + "text-align: center;" + "\n";
    	html = html + "padding: 50px;" + "\n";
    	html = html + "font-family: \"Open Sans\",\"Helvetica Neue\",Helvetica,Arial,sans-serif;" + "\n";
    	html = html + "}"+ "\n";
    	html = html + "#logo {"+ "\n";
    	html = html + "margin-bottom: 40px;" + "\n";
    	html = html + "}"+ "\n";
    	html = html + "</style>"+ "\n";
    	html = html + "</head>"+ "\n";
    	html = html + "<body>"+ "\n";
    	html = html + "<img id=\"logo\" src=\"http://www.uoc.edu/portal/_resources/common/imatges/marca_UOC/marca_UOC_blau_paper.png\" />"+ "<br>\n";
    	html = html + "Servidor: " + System.getenv("HOSTNAME")+ "<br>\n";
    	html = html + "Method called: " + methodCalled + "<br>\n";
    	html = html + "Result: " + resultToAdd + "\n";
    	html = html + "</body>"+ "\n";
    	html = html + "</html>"+ "\n"; 
    	return html;
    }

}
